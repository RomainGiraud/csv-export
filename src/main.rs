use clap::Parser;
use csv::ReaderBuilder;
use minijinja::Environment;
use minijinja::Error;
use minijinja::State;
use serde_json::{Map, Value};
use std::ffi::OsStr;
use std::fs;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;

/// Export a CSV file with a template
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Template file
    #[clap(short, long)]
    template: String,

    /// CSV file
    #[clap(short, long)]
    csv: String,

    /// Output filename (without extension) or output directory
    #[clap(short, long)]
    output: String,

    /// Output to a single file or multiple ones
    #[clap(short, long)]
    single: bool,

    /// First line is not header line
    #[clap(long)]
    no_headers: bool,
}

fn prefix(state: &State, prefix: String, var: String) -> Result<String, Error> {
    match state.lookup(&var) {
        Some(val) => Ok(prefix + val.as_str().unwrap() + "\n"),
        None => Ok("".to_string()),
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();

    println!("headers: {}", args.no_headers);

    let extension = Path::new(&args.template)
        .extension()
        .and_then(OsStr::to_str)
        .expect("Cannot get extension from template file");

    let mut file = None;
    if args.single {
        file = Some(
            OpenOptions::new()
                .write(true)
                .truncate(true)
                .create(true)
                .open(Path::new(&args.output))
                .expect("Cannot open or create output file"),
        );
    } else if !Path::new(&args.output).exists() {
        fs::create_dir(&args.output)?;
    } else if !Path::new(&args.output).is_dir() {
        panic!("Output file must be a directory");
    }

    let template = fs::read_to_string(&args.template).expect("Cannot read template file");

    let mut env = Environment::new();
    env.add_template("format", &template).unwrap();
    env.add_function("prefix", prefix);
    let template = env.get_template("format").unwrap();

    let mut rdr = ReaderBuilder::new()
        .has_headers(!args.no_headers)
        .from_reader(File::open(args.csv)?);

    let headers = if args.no_headers {
        None
    } else {
        Some(rdr.headers().cloned().unwrap())
    };

    for result in rdr.records() {
        let record = result.expect("Cannot read CSV record");

        let mut map = Map::new();
        if args.no_headers {
            for i in 0..record.len() {
                let val = record.get(i).unwrap().to_string();
                if !val.is_empty() {
                    map.insert(format!("column{}", i), Value::String(val));
                }
            }
        } else {
            for (i, hdr) in headers.as_ref().unwrap().iter().enumerate() {
                let val = record.get(i).unwrap().to_string();
                if !val.is_empty() {
                    map.insert(hdr.to_string(), Value::String(val));
                }
            }
        }

        let obj = Value::Object(map);
        let out = template.render(obj).unwrap();
        if args.single {
            if let Some(f) = &mut file {
                writeln!(f, "{}", out)?;
            }
        } else {
            let filename = record.get(0).unwrap().to_string();
            let f = String::from(&args.output) + "/" + &filename + "." + extension;
            println!("output: {}", f);
            let mut file = OpenOptions::new()
                .write(true)
                .truncate(true)
                .create(true)
                .open(f)
                .expect("File invalid");
            write!(file, "{}", out)?;
        }
    }

    Ok(())
}
