use assert_cmd::prelude::*; // Add methods on commands
use assert_fs::fixture::*;
use assert_fs::prelude::*;
use predicates::prelude::*; // Used for writing assertions
use std::process::Command; // Run programs

#[test]
fn single_output() -> Result<(), Box<dyn std::error::Error>> {
    let mut data = String::new();
    data += "display_name,last_name,first_name,nickname,zip,city,country,birthday\n";
    data += "Arthur Rimbaud,Rimbaud,Arthur,,08105,Charleville,France,18540920\n";
    data += "Jean-Baptiste Poquelin,Poquelin,Jean-Baptiste,Molière,75003,Paris,France,16220101\n";
    data += "Salvador Dali,Dali,Salvador,,,Figueras,Espagne,19040511\n";
    let csv = assert_fs::NamedTempFile::new("contacts.csv")?;
    csv.write_str(&data)?;

    let format = assert_fs::NamedTempFile::new("format.vcf")?;
    format.write_str("{{ first_name }}")?;

    let output = assert_fs::NamedTempFile::new("output.vcf")?;

    let mut cmd = Command::cargo_bin("csv-export")?;
    cmd.arg("-s")
        .arg("-o")
        .arg(output.path())
        .arg("-c")
        .arg(csv.path())
        .arg("-t")
        .arg(format.path());

    cmd.assert().success();

    let predicate_fn = predicate::str::diff("Arthur\nJean-Baptiste\nSalvador\n");
    output.assert(predicate_fn);

    Ok(())
}

#[test]
fn dir_output() -> Result<(), Box<dyn std::error::Error>> {
    let mut data = String::new();
    data += "display_name,last_name,first_name,nickname,zip,city,country,birthday\n";
    data += "Arthur Rimbaud,Rimbaud,Arthur,,08105,Charleville,France,18540920\n";
    data += "Jean-Baptiste Poquelin,Poquelin,Jean-Baptiste,Molière,75003,Paris,France,16220101\n";
    data += "Salvador Dali,Dali,Salvador,,,Figueras,Espagne,19040511\n";
    let csv = assert_fs::NamedTempFile::new("contacts.csv")?;
    csv.write_str(&data)?;

    let format = assert_fs::NamedTempFile::new("format.vcf")?;
    format.write_str("{{ first_name }} / {{ birthday }}")?;

    let output = assert_fs::TempDir::new()?;

    let mut cmd = Command::cargo_bin("csv-export")?;
    cmd.arg("-o")
        .arg(output.path())
        .arg("-c")
        .arg(csv.path())
        .arg("-t")
        .arg(format.path());

    cmd.assert().success();

    let person = output.child("Arthur Rimbaud.vcf");
    person.assert(predicate::str::diff("Arthur / 18540920"));

    let person = output.child("Jean-Baptiste Poquelin.vcf");
    person.assert(predicate::str::diff("Jean-Baptiste / 16220101"));

    let person = output.child("Salvador Dali.vcf");
    person.assert(predicate::str::diff("Salvador / 19040511"));

    Ok(())
}

#[test]
fn no_headers() -> Result<(), Box<dyn std::error::Error>> {
    let mut data = String::new();
    data += "Arthur Rimbaud,Rimbaud,Arthur,,08105,Charleville,France,18540920\n";
    data += "Jean-Baptiste Poquelin,Poquelin,Jean-Baptiste,Molière,75003,Paris,France,16220101\n";
    data += "Salvador Dali,Dali,Salvador,,,Figueras,Espagne,19040511\n";
    let csv = assert_fs::NamedTempFile::new("contacts.csv")?;
    csv.write_str(&data)?;

    let format = assert_fs::NamedTempFile::new("format.vcf")?;
    format.write_str("{{ column2 }} / {{ column3 }}")?;

    let output = assert_fs::NamedTempFile::new("output.vcf")?;

    let mut cmd = Command::cargo_bin("csv-export")?;
    cmd.arg("--no-headers")
        .arg("-s")
        .arg("-o")
        .arg(output.path())
        .arg("-c")
        .arg(csv.path())
        .arg("-t")
        .arg(format.path());

    cmd.assert().success();

    let predicate_fn = predicate::str::diff("Arthur / \nJean-Baptiste / Molière\nSalvador / \n");
    output.assert(predicate_fn);

    Ok(())
}
