<!-- omit in TOC -->
# csv-export

## About

Export each rows of a CSV file to a single (or multiple) files by using a template.

## Installation

```bash
cargo install csv-export
```

## Usage

```bash
csv-export --help
```

## Limitations

- This tool is not intended to alter CSV data. For this, you can use
  [qsv](https://crates.io/crates/qsv).
- Templating is done with [minijinja](https://crates.io/crates/minijinja) crate.

## Motivation

This tool was intended for my purpose only.
I used it to convert my contact list (exported from a mail provider in CSV) to valid and
clean VCF file.

I choosed to release it, so anyone can use it. Enjoy!
